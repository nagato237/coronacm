<?php
/**
 * Created by PhpStorm.
 * User: Franklin M
 * Date: 3/22/2020
 * Time: 5:40 PM
 */

namespace App\Controller;


use App\Entity\Cas;
use App\Entity\Ville;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{

    /**
     * @Route("/", name="homepage")
     */
    public function homecontroller(Request $request){

         $request->setLocale('en');

        $em = $this->getDoctrine()->getManager();

        $villes =$this->getDoctrine()->getRepository(Ville::class)->casparville();

        $cas = $this->getDoctrine()->getRepository(Cas::class)->nombreCas();
        $retablies = $this->getDoctrine()->getRepository(Cas::class)->casRetablis();
        $mort = $this->getDoctrine()->getRepository(Cas::class)->casMort();

        $nouveau = $this->getDoctrine()->getRepository(Cas::class)->casNouveau();

        $last = intval($cas) - intval($nouveau[0]["nombre"]);


        $diff = ((intval($cas) - $last)/$last)*100;
        if (is_float($diff)){
            $diff = round($diff, 2);
        }


        return $this->render('index.html.twig', ['nombre' =>$cas, 'retablies'=>$retablies, 'villes'=> $villes, 'mort'=>$mort, 'nouveau'=>$nouveau, 'diff'=> $diff]);
    }


    /**
     * @Route("contact", name="contact")
     */
    public function contact(){
       return $this->render('contact.html.twig');
    }

    /**
     * @Route("newsletter", name="newsletter")
     */
    public function newsletter(){
        return $this->render('newsletter.html.twig');
    }

    /**
     * @Route("infos", name="infos")
     */
    public function infos(){
        return $this->render('infos.html.twig');
    }

    /**
     * @Route("stats", name="stats")
     */
    public function stats(){
        return $this->render('stats.html.twig');
    }

    /**
     * @Route("actualites", name="actualites")
     */
    public function actualites(){
        return $this->render('actualites.thtml.twig');
    }


    /**
     * @Route("casparville" , name="casparville", methods={"GET"})
     */
    public function casparville(Request $request){
        $response = new JsonResponse();

            $data= $this->getDoctrine()->getRepository(Ville::class)->casparville();

            return $response->setData($data);

    }

    /**
     * @Route("casparjour" , name="casparjour", methods={"GET"})
     */
    public function casparjour(Request $request){
        $response = new JsonResponse();

        $data= $this->getDoctrine()->getRepository(Cas::class)->casparJour();

        return $response->setData($data);

    }
}