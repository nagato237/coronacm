<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VilleRepository")
 */
class Ville
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="float")
     */
    private $longtitude;

    /**
     * @ORM\Column(type="float")
     */
    private $lattitude;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Cas", mappedBy="ville")
     */
    private $cas;

    public function __construct()
    {
        $this->cas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getLongtitude(): ?int
    {
        return $this->longtitude;
    }

    public function setLongtitude(int $longtitude): self
    {
        $this->longtitude = $longtitude;

        return $this;
    }

    public function getLattitude(): ?int
    {
        return $this->lattitude;
    }

    public function setLattitude(int $lattitude): self
    {
        $this->lattitude = $lattitude;

        return $this;
    }

    /**
     * @return Collection|Cas[]
     */
    public function getCas(): Collection
    {
        return $this->cas;
    }

    public function addCa(Cas $ca): self
    {
        if (!$this->cas->contains($ca)) {
            $this->cas[] = $ca;
            $ca->setVille($this);
        }

        return $this;
    }

    public function removeCa(Cas $ca): self
    {
        if ($this->cas->contains($ca)) {
            $this->cas->removeElement($ca);
            // set the owning side to null (unless already changed)
            if ($ca->getVille() === $this) {
                $ca->setVille(null);
            }
        }

        return $this;
    }
}
