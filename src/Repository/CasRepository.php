<?php

namespace App\Repository;

use App\Entity\Cas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Cas|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cas|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cas[]    findAll()
 * @method Cas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Cas::class);
    }

    // /**
    //  * @return Cas[] Returns an array of Cas objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /**
     * @return int
     */
        public function  nombreCas(){
            return $this->createQueryBuilder('c')
                ->select('SUM(c.nombre)')
                ->where('c.status = :code')
                ->setParameter('code', 1)
                ->getQuery()->getSingleScalarResult();
        }

    /**
     * @return int
     */
    public function  casRetablis(){
        return $this->createQueryBuilder('c')
            ->select('SUM(c.nombre)')
            ->where('c.status = :code')
            ->setParameter('code', 3)
            ->getQuery()->getSingleScalarResult();
    }

    public function  casMort(){
        return $this->createQueryBuilder('c')
            ->select('SUM(c.nombre)')
            ->where('c.status = :code')
            ->setParameter('code', 2)
            ->getQuery()->getSingleScalarResult();
    }

    public function  casNouveau(){

        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT SUM(nombre) as nombre, DATE_FORMAT(jour, "%D %b %Y") as jourformat FROM cas WHERE status_id= 1 GROUP BY jour ORDER BY jour DESC LIMIT 2  ';
        $query = $conn->prepare($sql);
        $query->execute([]);
        return $query->fetchAll();

    }

    public function  casparJour(){

        $conn = $this->getEntityManager()->getConnection();
        $sql = 'SELECT DATE_FORMAT(jour, "%D %b %Y") as jourformat, sum(cas.nombre) as nombre FROM cas WHERE status_id = 1 GROUP BY cas.jour ORDER BY jour DESC LIMIT 7 ';
        $query = $conn->prepare($sql);
        $query->execute([]);
        return $query->fetchAll();

    }
    /*
    public function findOneBySomeField($value): ?Cas
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
